from config import *
from pathlib import Path
from os.path import basename, dirname, splitext, isdir
from shutil import copy2
from os import makedirs

def happy(_val):
    try:
        return float(_val)
    except ValueError:
        return float(_val.replace(",", '.'))
    except:
        raise

def adjust_int_type(_val):
    if int(_val) == _val:
        return int(_val)
    else:
        return _val
    
def read_and_replace_template(_data, _filename, _builddir):
    with open(tex_templates_path + "/" + _filename) as f:
        tex = f.read()
    for i in _data.keys():
        tex = tex.replace(i, str(_data[i]))
        
    print(f'Generating {_filename}')
    with open(f'{_builddir}/{_filename}', 'w') as f:
        f.write(tex)
        f.close()
    
def prepare_personal_math(_person_id, _builddir):
    with open(tex_templates_path + "/" + 'p-math.tex') as f:
        tex = f.read()
        
    scans = []
    for filename in Path(math_path + '/' + str(_person_id)).glob('**/*'):
        scan_name = basename(filename)
        if (scan_name != title_paper_page) and (scan_name != participant_photo_filename):
            scans.append(scan_name)
            print(f'Copying {scan_name}')
            copy2(filename, _builddir)
    
    scans.sort()

    if len(scans) == 0:
        tex = tex + '\n\n\\noMath'
    # \noMath
    else:
        for i, fname in enumerate(scans):
            tex = tex + '\n\n\\newListWithPaper{' + str(i+1) + '}{' + fname + '}'
    
    print(f'Generating p-math.tex')
    with open(f'{_builddir}/p-math.tex', 'w') as f:
        f.write(tex)
        f.close()
        
def prepare_personal_inf(_person_id, _builddir):
    with open(tex_templates_path + "/" + 'p-inf.tex') as f:
        tex = f.read()
        
    solutions = {}
    for filename in Path(inf_path + '/' + str(_person_id)).glob('**/*'):
        solution_file = basename(filename)
        task, lang = splitext(solution_file)
        solutions[task] = (lang[1:], solution_file)
        print(f'Copying {solution_file}')
        copy2(filename, _builddir)

    tasks = list(tasks_inf.keys())
    tasks.sort()
    for i, task in enumerate(tasks):
        if task in solutions:
            tex = tex + f'\n\n\\{inf_ext_to_tex[solutions[task][0]]}{{{i+1}}}{{{solutions[task][1]}}}'
        else:
            tex = tex + f'\n\n\\noSource{{{i+1}}}'
            
    tex = tex + f'\n\pagebreak'

    print(f'Generating p-inf.tex')
    with open(f'{_builddir}/p-inf.tex', 'w') as f:
        f.write(tex)
        f.close()

def prepare_teammates_list(_persons, _team_id, _builddir):
    with open(tex_templates_path + "/" + 'teammates.tex') as f:
        tex = f.read()
        
    mates = [i[1]['name'] for i in _persons.loc[_persons['teamcode'] == _team_id].iterrows()]
    to_replace = ''
    for i in mates:
        to_replace = to_replace + f'\n\t\item {i}'
        
    tex = tex.replace('teammates_list', to_replace)

    print(f'Generating teammates.tex')
    with open(f'{_builddir}/teammates.tex', 'w') as f:
        f.write(tex)
        f.close()
        
def prepare_team_scoring_per_phase(_phases, _phase_num, _team_id, _builddir):
    phase_file_name = phases_tex_templates[_phase_num]
    with open(tex_templates_path + "/" + phase_file_name) as f:
        tex = f.read()
        
    s = 0
    to_replace = ''
    for i in _phases.loc[_phases['phase'] == _phase_num, ['task', 'maxpoints', _team_id]].iterrows():
        task = i[1]["task"]
        maxp = adjust_int_type(i[1]["maxpoints"])
        points = adjust_int_type(i[1][_team_id])
        s = s + points
        to_replace = to_replace + f'\n\oneLineInTasksList{{{task}}}{{{maxp}}}{{{points}}}'
    to_replace = to_replace + f'\n\lineWithTotalPerPhase{{{adjust_int_type(s)}}}'
        
    tex = tex.replace('tasks_table', to_replace)

    print(f'Generating {phase_file_name}')
    with open(f'{_builddir}/{phase_file_name}', 'w') as f:
        f.write(tex)
        f.close()
        
def prepare_sources_list(_team_id, _builddir):
    with open(tex_templates_path + "/" + 'team-code.tex') as f:
        tex = f.read()

    path_to_sources = teams_path + '/' + str(_team_id)
    for i in team_solution_extensions:
        for filename in Path(path_to_sources + '/' + team_solution_path).glob('**/'+i):
            task, lang = splitext(basename(filename))
            source = str(filename).replace(path_to_sources + "/", '')
            tex_command = sources_ext_to_tex[lang[1:]]
            
            corrected_source = source.replace('_', '-')
            
            tex = tex + f'\n\\{tex_command}{{{corrected_source}}}'
            dirs = dirname(source)
            while dirs != '':
                srcdir = f'{_builddir}/{dirs}'
                if str(dirs).find('_') != -1:
                    srcdir = srcdir.replace('_', '-')
                if not isdir(srcdir):
                    print(f'Creating {dirs}')
                    makedirs(srcdir, exist_ok=True)
                dirs = dirname(dirs)
                
            print(f'Copying {source}')
            dest = str(filename).replace(path_to_sources + '/' + team_solution_path + '/', '')
            corrected_dest = dest.replace('_', '-')
            copy2(filename, f'{_builddir}/{team_solution_path}/{corrected_dest}')
        
    print(f'Generating team-code.tex')
    with open(f'{_builddir}/team-code.tex', 'w') as f:
        f.write(tex)
        f.close()