#!/bin/bash

MAINTEXFILE='main.tex'

BUILDDIRNAME='build'
FONTCACHEDIRNAME='font-cache'

FONTCACHE_IN_DOCKER='/root/.texlive2019'

BUILDSCRIPTPATH_IN_DOCKER='./build_in_docker.sh'
MOUNTPOINT_IN_DOCKER='/mnt'
WORKDIR_IN_DOCKER=${MOUNTPOINT_IN_DOCKER}

if [ -f /.dockerenv ]; then
    echo '---=== CONTAINERED ===---'

    IFS=$'\n'
    find -maxdepth 1 -mindepth 1 -type d | while read i
    do
        person_id=`echo ${i} | cut -c3-`
        echo "--------------------------------------------"
        echo "preparation PDF for ${person_id} started"
        echo "--------------------------------------------"
        cd ${person_id}

        echo "Run PDF compilation"
        latexmk -shell-escape -pdf ${MAINTEXFILE}
        if [ "$?" == "0" ]; then
            echo "--------------------------------------------"
            echo "PDF for ${person_id} prepared"
            echo "--------------------------------------------"
            mv main.pdf ../${person_id}.pdf
        fi

        cd ..
    done
    exit 0
fi

echo '---=== STILL ===---'

CURDIR=`pwd`
BUILDDIRPATH=${CURDIR}/${BUILDDIRNAME}
FONTCACHEDIRPATH=${CURDIR}/${FONTCACHEDIRNAME}

if [ "${1}" == "" ]; then
    if [ ! -d ${FONTCACHEDIRPATH} ]; then
        echo "Create font cache directory"
        mkdir ${FONTCACHEDIRPATH}
    fi

    cp ${BUILDSCRIPTPATH_IN_DOCKER} ${BUILDDIRPATH}

    echo "Run docker container"
    docker run --rm -it -v ${BUILDDIRPATH}/:${MOUNTPOINT_IN_DOCKER} \
        -v ${FONTCACHEDIRPATH}:${FONTCACHE_IN_DOCKER} \
        -w ${WORKDIR_IN_DOCKER} iustem/latex ${BUILDSCRIPTPATH_IN_DOCKER}
elif [ "${1}" == "clean" ]; then
    if [ -d ${FONTCACHEDIRPATH} ]; then
        echo "Remove font-cache directory"
        rm -rf ${FONTCACHEDIRPATH}
    fi
else
    echo "Incorrect usage"
    exit 1
fi