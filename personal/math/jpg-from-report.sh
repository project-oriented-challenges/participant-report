#!/bin/bash

# Данный скрипт позволяет автоматизировать извлечение отсканированных страниц
# из PDF в JPG. Для его работы используется pdfimages из пакета poppler-utils.
#
# poppler-utils могут быть собраны в Docker образ с помощью Dockerfile,
# располагающемся в этой же директории.
#
# Ожидаемая структура дерева с файлами:
# <participant code> (dir, e.g. 1001)
# |--<source of images> (pdf file, e.g. participant1001.pdf)
#
# PDF файл после извлечения изображений удаляется. В итоге получается дерево
# файлов, понимаемое генератором отчета
# <participant code> (dir, e.g. 1001)
# |--math-000.jpg
# |--math-001.jpg
# |. . .
# |--math-NNN.jpg

IFS=$'\n'
find . -mindepth 1 | grep '.pdf' | while read i
do
	dirn=`dirname ${i}`
	echo ${dirn}
    n=`basename ${i}`
	cd ${dirn}
	pdfimages -all ${n} math
	rm "${n}"
	cd ..
done
