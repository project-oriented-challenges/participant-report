#!/bin/bash

# Данный скрипт позволяет упростить дерево с решениями от участников, которое
# отдает PCMS.
#
# Структура дерева с решениями:
# <participant code> (dir, e.g. 001)
# |--<attempt with task id> (dir, e.g. A.3.879416)
#    |--sources (dir)
#       |--<actual solution> (file, e.g. somename.cpp)
#
# Подразумевается, что
# а) <task id> -- один символ: например в названии A.3.879416 <A> - task id
# б) для каждого участника оставлено по одному решению (посылке) на каждую задачу.
#
# В итоге получится следующее дерево, ожидаемое генератором отчета:
# <participant code> (dir, e.g. 001)
# |--actual solution (file, e.g. task.cpp)

IFS=$'\n'
find . -maxdepth 1 -mindepth 1 -type d | while read i
do
	dirn=${i}
	echo ${i}
	cd ${dirn}
	find . -maxdepth 1 -mindepth 1 -type d | while read j
	do
		newfilename=`basename ${j} | cut -c1`
		cd ${j}
		find . -type f | while read j
		do
			filename=$(basename -- "${j}")
			extension="${filename##*.}"
			# echo mv ${j} ../${newfilename}.${extension}
			mv ${j} ../${newfilename}.${extension}
		done
		cd ..
		# echo rm -rf ${j}
		rm -rf ${j}
	done
	cd ..
done