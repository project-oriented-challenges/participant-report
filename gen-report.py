#!/usr/bin/env python

from config import *
from utils import *
import pandas as pd
from os.path import isdir, isfile
from shutil import rmtree
from os import makedirs
import sys

if len(sys.argv) != 3:
    print("Usage: gen-report.py <data file> <scoring file>")
    sys.exit(1)

if isfile(sys.argv[1]):
    default_data_csv = sys.argv[1]
    try:
        persons = pd.read_csv(default_data_csv, sep=csv_saparator, header=0)
    except:
        print("Cannot use CSV parser for the data file")
        sys.exit(2)
else:
    print("Usage: gen-report.py <data file> <scoring file>")
    sys.exit(3)

if isfile(sys.argv[2]):
    default_phases_csv = sys.argv[2]
    try:
        phases = pd.read_csv(default_phases_csv, sep=csv_saparator, header=0)
    except:
        print("Cannot use CSV parser for the scoring file")
        sys.exit(4)
else:
    print("Usage: gen-report.py <data file> <scoring file>")
    sys.exit(5)

persons = persons.fillna(0)
phases = phases.fillna(0)

phases_number = phases['phase'].max()
            
print("Preparing build directory")
if isdir(builddir):
    print(f'directory {builddir} exists, removing')
    rmtree(builddir)
makedirs(builddir)
    
for i in persons.loc[persons['flag'] == 1].iterrows():
    data = {'track_phases_number': phases_number,
            'track_name'         : track_name,
            'title_paper_page'   : title_paper_page,
            'team_photo'         : team_photo_filename,
            'person_name'        : i[1]['name'],
            'person_year'        : int(i[1]['year']),
            'person_school'      : i[1]['school'],
            'person_id'          : int(i[1]['codemath']),
            'person_group'       : i[1]['group'],
            'person_city'        : i[1]['city'],
            'person_region'      : i[1]['region'],
            'codeinf'            : int(i[1]['codeinf']),
            'team_id'            : i[1]['teamcode']
           }
    
    teamname = i[1]['teamname'].replace("\\", r'\char`\\')
    data['person_team'] = teamname.replace('_', '-')


    data['person_math'] = adjust_int_type(i[1]['mathtask1']+
                                          i[1]['mathtask2']+
                                          i[1]['mathtask3']+
                                          i[1]['mathtask4']+
                                          i[1]['mathtask5']
                                          )
    try:
        data['person_inf'] = adjust_int_type(round(happy(i[1]['inftask1']) + 
                                                   happy(i[1]['inftask2']) + 
                                                   happy(i[1]['inftask3']) +
                                                   happy(i[1]['inftask4']) +
                                                   happy(i[1]['inftask5'])
                                                   , 1))
    except:
        #print(e)
        print(i[1]['inftask1'], i[1]['inftask2'], i[1]['inftask3'])
        raise
    data['person_total'] = adjust_int_type(data['person_math'] + data['person_inf'])
    phases_total = 0
    for j in range(1, phases_number+1):
        s = phases.loc[phases['phase'] == j, [data['team_id']]][data['team_id']].sum()
        phases_total = phases_total + s
        data[f'team_phase_{j}_total'] = adjust_int_type(s)
    data['team_phases_total'] = adjust_int_type(phases_total)
    data['track_total'] = adjust_int_type(data['person_total'] + data['team_phases_total'])
    
    person_builddir = f'{builddir}/{data["person_id"]}'
    
    print(f'Preparing personal build directory for {data["person_id"]}')
    makedirs(person_builddir)
    print("Coping main.tex")
    copy2(f'{tex_templates_path}/main.tex', person_builddir)
    print("Coping team-scoring.tex")
    copy2(f'{tex_templates_path}/team-scoring.tex', person_builddir)
    print(f"Coping {resulting_protocol_path}")
    copy2(f'{resulting_protocol_path}', person_builddir)
    print(f'Coping {participant_photo_filename}')
    copy2(f'{math_path}/{data["person_id"]}/{participant_photo_filename}', person_builddir)
    try:
        print(f'Coping {title_paper_page}')
        copy2(f'{math_path}/{data["person_id"]}/{title_paper_page}', person_builddir)
    except Exception as e:
        pass
    print(f'Coping {team_photo_filename}')
    copy2(f'{teams_path}/{data["team_id"]}/{team_photo_filename}', person_builddir)
    
    read_and_replace_template(data, "configuration.tex", person_builddir)
    read_and_replace_template(data, "titlepage.tex", person_builddir)
    if phases_number == 3:
        read_and_replace_template(data, "3-phases-results-table.tex", person_builddir)
    else:
        read_and_replace_template(data, "4-phases-results-table.tex", person_builddir)
    read_and_replace_template(data, "personal.tex", person_builddir)
    prepare_personal_math(data['person_id'], person_builddir)
    prepare_personal_inf(data['codeinf'], person_builddir)
    prepare_teammates_list(persons, data['team_id'], person_builddir)
    read_and_replace_template(data, "team.tex", person_builddir)
    for j in range(1, phases_number+1):
        prepare_team_scoring_per_phase(phases, j, data['team_id'], person_builddir)
    prepare_sources_list(data['team_id'], person_builddir)
    print()